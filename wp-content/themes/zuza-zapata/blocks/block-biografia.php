<section class="py-2 py-lg-5 text-white" style="background-image: url('<?php echo get_template_directory_uri(); ?>/src/imgs/zuza-2.jpg'); background-size: cover; background-position: center center;">
    <div class="container py-5">
        <div class="row py-5">
            <div class="col-lg-6">
                <div class="section-title">
                    <h2 class="h1 text-uppercase text-white skip-contrast">Biografia</h2>
                </div>
                <p class="lead py-4 text-white skip-contrast">
                    <?php 
                        $content = get_post_field( 'post_content', 28 );
                        $content_parts = get_extended($content);
                        echo $content_parts['main'];
                    ?>                    
                </p>
                <a href="" class="btn btn-primary btn-lg">Saiba mais »</a>
            </div>
        </div>
    </div>
</section>