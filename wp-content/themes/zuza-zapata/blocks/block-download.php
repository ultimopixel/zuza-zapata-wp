<section class="py-2 py-lg-5">
    <div class="container py-5">
        <div class="row align-items-center">
            <div class="col-sm-5 mb-5 mb-sm-0">
                <div class="section-title">
                    <h2 class="text-uppercase">Download</h2>
                    <img src="<?php echo get_the_post_thumbnail_url(47); ?>" class="w-100">                    
                </div>
            </div>
            <div class="col-sm-5 offset-sm-2">
                <h3 class="mb-5">
                    <?php echo get_the_content(null,false,47); ?>
                </h3>
                <form action="https://paginas.rocks/pages/index/360211" method="post" >
                    <input id="id" name="id" type="hidden" value="360211" />
                    <input id="mid" name="mid" type="hidden" value="360211" />
                    <input id="pid" name="pid" type="hidden" value="18113706" />
                    <input id="list_id" name="list_id" type="hidden" value="360211" />
                    <input id="provider" name="provider" type="hidden" value="leadlovers" />
                    <label for="email">E-mail:</label>
                    <input class="form-control form-ll" id="email" name="email" placeholder="Informe o seu email" type="text" />
                    <label for="name">Nome:</label>
                    <input class="form-control form-ll" id="name" name="name" placeholder="Informe o seu nome" type="text"  />
                    <button class="btn btn-danger" style="padding: 10px 40px; margin:15px 0 5px;  " type="submit">DOWNLOAD</button>
                    <input type="hidden" id="source" name="source" value="" />
                    <img src="https://llimages.com/redirect/redirect.aspx?A=V&p=18113706&m=360211" style="display: none;" />
                </form>            
            </div>
        </div>
    </div>
</section>