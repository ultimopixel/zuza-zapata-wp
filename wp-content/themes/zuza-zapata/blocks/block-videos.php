<section class="bg-dark py-2 py-lg-5">
    <div class="container py-5">
        <div class="row mb-5">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="text-white">Vídeos</h2> 
                </div>                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php  
                                $videos = [];                                             
                                $the_query = new WP_Query([
                                    'post_type' => 'videos',
                                    'posts_per_page' => -1
                                ]);
                                if ( $the_query->have_posts() ) :
                                    while ( $the_query->have_posts() ) : $the_query->the_post();
                                        $videos[] = get_field('video_url', get_the_ID());
                                    endwhile;
                                endif; wp_reset_postdata();
                            ?>
                            <?php foreach($videos as $video){ ?>
                                <?php 
                                    $oembed = json_decode(file_get_contents('https://www.youtube.com/oembed?url='.urlencode($video).'&format=json'), true);
                                ?>
                                <div class="swiper-slide">
                                    <a data-fancybox="gallery" href="<?php echo $video; ?>">
                                        <div class="card video" style="background-image: url('<?php echo $oembed['thumbnail_url']; ?>'); background-size: 190%; background-position: center center;">
                                            <img src="<?php echo $oembed['thumbnail_url']; ?>" class="card-img" style="opacity: 0;">
                                            <div>
                                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="play-circle" class="svg-inline--fa fa-play-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M371.7 238l-176-107c-15.8-8.8-35.7 2.5-35.7 21v208c0 18.4 19.8 29.8 35.7 21l176-101c16.4-9.1 16.4-32.8 0-42zM504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256z"></path></svg>
                                            </div>
                                        </div>    
                                    </a>                        
                                </div>
                            <?php } ?>
                        </div> 
                        <div class="swiper-pagination d-none"></div>                        
                    </div>
                </div>
            </div>                      
        </div>
    </div>
</section>