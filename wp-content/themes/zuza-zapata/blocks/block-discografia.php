<a name="content"></a>
<a name="discografia" class="discografia"></a>
<section class="bg-dark py-2 py-lg-5">
    <div class="container py-5">
        <div class="row mb-5">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="text-white">Discografia</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper" data-slides-per-view="2">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">

                            <?php                                               
                            $the_query = new WP_Query([
                                'post_type' => 'discografia',
                                'posts_per_page' => 2
                            ]);
                            ?>

                            <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="card-img">
                                            <div class="p-0 text-white card-img-overlay d-flex flex-column justify-content-between">
                                                <div class="p-3" style="opacity: 0;">
                                                                               
                                                </div>
                                                <div class="pl-3">
                                                    <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais »</a>
                                                </div>
                                            </div>
                                        </div>                            
                                    </div>
                                <?php endwhile; ?>  
                            <?php endif; wp_reset_postdata(); ?>                              
                          
                        </div>
                        <div class="swiper-pagination d-none"></div>  
                    </div>
                </div>
            </div>                      
        </div>
    </div>
</section>