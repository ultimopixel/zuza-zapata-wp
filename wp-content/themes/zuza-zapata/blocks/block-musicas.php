<section class="py-2 py-lg-5 bg-dark text-white">
    <div class="container py-5">
        <div class="row align-items-center">
            <div class="col-sm-5 mb-5 mb-sm-0">
                <div class="section-title">
                    <h2 class="text-uppercase text-white">Músicas</h2>
                </div>
                <p class="py-lg-5">
                    <?php echo get_field('musics_texto', 45); ?>
                </p>
                <?php echo get_field('spotify_like_embed', 45); ?>
            </div>
            <div class="col-sm-6 offset-sm-1">
            <?php echo get_field('spotify_playlist_embed', 45); ?>
            </div>
        </div>
    </div>
</section>