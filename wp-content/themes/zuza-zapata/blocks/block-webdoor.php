<section class="webdoor">
    <div class="swiper-container">
        <div class="swiper-wrapper p-0">
            <?php if( have_rows('webdoors') ): ?>
                <?php while( have_rows('webdoors') ) : the_row(); ?>
                    <div class="swiper-slide">                
                        <div class="slide-content" style="background-image: url('<?php echo get_sub_field('webdoor_image'); ?>');">      
                            <div>
                                <h2 class="h1">
                                    <?php echo get_sub_field('webdoor_title'); ?>
                                </h2>
                                <p class="lead">
                                    <?php echo get_sub_field('webdoor_texto'); ?>
                                </p>
                                <a href="<?php echo home_url(get_sub_field('webdoor_link')); ?>" class="btn btn-primary btn-lg">Saiba mais »</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>                                           
        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>