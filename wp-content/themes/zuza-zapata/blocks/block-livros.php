<section class="bg-white py-2 py-lg-5 text-center text-sm-left">
    <div class="container py-5">
        <div class="row mb-sm-5">
            <div class="col-lg-6 mx-auto">
                <div class="text-center">
                    <h2 class="h1 text-uppercase text-dark d-inline-block text-white">Livros</h2>                    
                </div>
            </div>
        </div>

        <?php                                               
        $the_query = new WP_Query([
            'post_type' => 'livros',
            'posts_per_page' => 2
        ]);
        ?>

        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="row align-items-center">
                    <div class="col-sm-6 order-lg-1">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="w-100">
                    </div>                      
                    <div class="col-sm-5 order-lg-2">
                        <h2><?php echo get_the_title(); ?></h2>
                        <p class="py-3">
                            <?php echo get_the_content(); ?>
                        </p>
                        <a href="" class="btn btn-primary btn-lg">Comprar »</a>
                    </div>
                </div>
                <div class="row pt-5 py-sm-5 d-block d-lg-none">
                    <div class="col-lg-12">
                        <hr />
                    </div>
                </div>                
            <?php endwhile; ?>  
        <?php endif; wp_reset_postdata(); ?>          
      
    </div>
</section>