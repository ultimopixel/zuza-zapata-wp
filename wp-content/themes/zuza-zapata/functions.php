<?php  

add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_theme_support( 'widgets' );	
add_theme_support( 'editor' );	

add_action( 'after_setup_theme', 'up_setup' );
function up_setup(){
	// add_image_size( 'medium', 655, 425, true );	
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Zuza Settings',
		'menu_title'	=> 'Zuza Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
function prefix_disable_gutenberg($current_status, $post_type)
{
	$exclude_post_types = ['discografia', 'videos', 'livros', 'products'];
    if (in_array($post_type, $exclude_post_types)) return false;
    return $current_status;
}

add_filter('use_block_editor_for_post', 'prefix_disable_gutenberg_for_post', 11, 2);
function prefix_disable_gutenberg_for_post($current_status, $post)
{
	if($post->post_name == 'home'){
		return true;
	}
	if($post->post_name == 'musicas'){
		return false;
	}	
	if($post->post_name == 'contato'){
		return false;
	}		
    return $current_status;
}

if(function_exists('acf_register_block_type')){
	add_action('acf/init', 'register_acf_block_types');
}
function register_acf_block_types(){

	acf_register_block_type( array(
		'name'			=> 'Webdoor',
		'title'			=> __( 'Webdoor'),
		'render_template'	=> 'blocks/block-webdoor.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'webdoor' )
	));		

	acf_register_block_type( array(
		'name'			=> 'Discografia',
		'title'			=> __( 'Discografia'),
		'render_template'	=> 'blocks/block-discografia.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'discografia' )
	));		

	acf_register_block_type( array(
		'name'			=> 'Biografia',
		'title'			=> __( 'Biografia'),
		'render_template'	=> 'blocks/block-biografia.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'biografia' )
	));	

	acf_register_block_type( array(
		'name'			=> 'Videos Block',
		'title'			=> __( 'Vídeos Block'),
		'render_template'	=> 'blocks/block-videos.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'videos' )
	));	
	
	acf_register_block_type( array(
		'name'			=> 'Livros',
		'title'			=> __( 'Livros'),
		'render_template'	=> 'blocks/block-livros.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'livros' )
	));	
	
	acf_register_block_type( array(
		'name'			=> 'Musicas',
		'title'			=> __( 'Musicas'),
		'render_template'	=> 'blocks/block-musicas.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'musicas' )
	));	
	
	acf_register_block_type( array(
		'name'			=> 'Download',
		'title'			=> __( 'Download'),
		'render_template'	=> 'blocks/block-download.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'download' )
	));		

}