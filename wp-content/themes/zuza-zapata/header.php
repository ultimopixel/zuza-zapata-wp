<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Zuza Zapata</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <!-- Styles -->
    <link rel="stylesheet" href="https://blob.contato.io/machine-files/all-css/form-ll.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/app.css">
    
  </head>
  <body>

    <section class="accessibility bg-secondary py-3 text-white">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-6 col-md-auto text-md-center text-sm-left">
            <a href="#content" class="pr-3 text-white small" accesskey="1">
              <span class="d-none d-md-inline-block">Ir para o</span> conteúdo [1]
            </a>
            <a href="#menu" class="text-white small" accesskey="2"> 
              <span class="d-none d-md-inline-block">Ir para o</span> menu [2]
            </a>
          </div>
          <div class="col-3 col-md-auto">
            <div class="d-flex align-items-center justify-content-center">
              <a href="javascript:void(0);" class="text-white" data-change-font-size="default">A-</a>
              <a href="javascript:void(0);" class="text-white lead" data-change-font-size="large">A+</a>
            </div>
          </div>          
          <div class="col-3 col-md-auto">
            <div class="d-flex align-items-center justify-content-center">
              
              <a href="javascript:void(0);" class="contrast_option contrast_default mr-1" data-change-contrast="default"></a>
              <a href="javascript:void(0);" class="contrast_option contrast_white" data-change-contrast="contrast"></a>
            </div>               
          </div>
        </div>
      </div>
    </section>

    <a name="menu"></a>
    <header class="<?php echo (!is_front_page()) ? 'bg-secondary' : ''; ?>">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <nav class="navbar navbar-expand-lg navbar-dark p-0">
              <a class="navbar-brand <?php echo (!is_front_page()) ? 'text-white' : ''; ?>" href="<?php echo home_url(); ?>">ZUZA ZAPATA</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <?php global $wp; ?>         
                  <?php $main_menu_items = wp_get_nav_menu_items('main-menu'); ?>
                  <?php foreach($main_menu_items as $item){ ?>
                    <li class="nav-item <?php echo (strpos($item->url, $wp->request) !== false) ? 'active' : ''; ?>">
                          <a href="<?php echo $item->url; ?>" class="nav-link <?php echo (!is_front_page()) ? 'text-white' : ''; ?>">
                              <?php echo $item->title; ?>                  
                          </a>
                      </li>
                  <?php } ?>                                                                                                                                                
                </ul>
              </div>
            </nav>          
          </div>
        </div>
      </div>      
    </header>    