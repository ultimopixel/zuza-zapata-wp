<?php get_header(); ?>
<?php the_post(); ?>	

<section class="bg-dark text-white py-3 py-lg-5">
    <div class="container py-lg-5">
        <div class="row align-items-center">
            <div class="col-lg-5 mb-4 mb-lg-0">
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="w-100">
            </div>
            <div class="col-lg-6 offset-lg-1">
                <div class="section-title mb-4">
                    <h2><?php echo get_the_title(); ?></h2>
                </div>
                <?php echo the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>