'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require("gulp-rename");

gulp.task('default', function () {
    gulp.watch('./src/sass/**/*.scss', gulp.series('workflow-css'));
    gulp.watch('./src/js/custom.js', gulp.series('compress-custom-js'));
});

gulp.task('workflow-css', function () {
   return gulp.src('./src/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
    }))
    .pipe(cssnano())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('compress-custom-js', function () {
    return gulp.src([
        './src/js/custom.js'
    ])    
    .pipe(concat('custom.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));    
});

gulp.task('compress-base-js', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/swiper/swiper-bundle.js'
    ])    
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));    
});