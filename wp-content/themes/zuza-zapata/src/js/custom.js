$(document).ready(function(){

    /*
    $('[data-scroll-to]').click(function(e){
        var el = $(this).attr('data-scroll-to');
        if($('a[name='+el+']')[0]){
            $('html,body').animate({
                scrollTop: $('a[name='+el+']').offset().top
            }, 500);
        }else if($(el)[0]){
            $('html,body').animate({
                scrollTop: $(el).offset().top
            }, 500);
        }
        e.preventDefault();
    });  
    */

    $('[data-change-font-size]').click(function(){
        var size = $(this).attr('data-change-font-size');
        $('body').removeClass('default');
        $('body').removeClass('large');
        $('body').addClass(size);        
    });

    $('[data-change-contrast]').click(function(){
        var contrast = $(this).attr('data-change-contrast');
        $('body').removeClass('default');
        $('body').removeClass('contrast');
        $('body').addClass(contrast);
    });    

    var swiper = new Swiper('.webdoor .swiper-container', {
        // direction: 'vertical',
        pagination: {
            el: '.webdoor .swiper-pagination',
            clickable: true,
        },
    });
    
    $('.swiper').each(function(){

        var slidesPerViewMobile = 1.3;
        if($(this).attr('data-slides-per-view-mobile') !== undefined){
            slidesPerViewMobile = $(this).attr('data-slides-per-view-mobile');
        }    

        var slidesPerView = 3;
        if($(this).attr('data-slides-per-view') !== undefined){
            slidesPerView = $(this).attr('data-slides-per-view');
        }

        var spaceBetween = 30;
        if($(this).attr('data-space-between') !== undefined){
            spaceBetween = $(this).attr('data-space-between');
        }   
        
        var spaceBetweenMobile = 30;
        if($(this).attr('data-space-between-mobile') !== undefined){
            spaceBetweenMobile = $(this).attr('data-space-between-mobile');
        }      

        var swiper = new Swiper($(this).find('.swiper-container')[0], {
            scrollbar: {
                el: $(this).find('.swiper-scrollbar')[0],
                draggable: true
            },          
            slidesPerView: slidesPerViewMobile,
            spaceBetween: 10,
            pagination: {
                el: $(this).find('.swiper-pagination')[0],
                clickable: true,
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                1140: {
                    slidesPerView: slidesPerView,
                    spaceBetween: spaceBetween,
                },
            },
        });
            
    });


});