<?php /* Template Name: Page Galeria de fotos */ ?>
<?php get_header(); ?>
<?php the_post(); ?>

<section class="bg-dark py-5">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2 class="h1 text-uppercase text-white skip-contrast">Galeria de fotos</h2>
                </div>            
            </div>
        </div>
        <div class="row">
            <?php 
            $images = get_field('gallery'); 
            $size = 'full';
            ?>
            <?php foreach( $images as $image ): ?>
                <div class="col-lg-3">
                    <a data-fancybox="gallery" href="<?php echo $image; ?>">
                        <img src="<?php echo $image; ?>" class="w-100">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>